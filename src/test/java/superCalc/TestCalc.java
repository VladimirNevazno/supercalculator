package superCalc;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import superCalc.Сalc;

public class TestCalc {

	@BeforeClass
	public static void setUpBeforeClass() {
	}

	@AfterClass
	public static void tearDownAfterClass() {
	}

	@Before
	public void setUp() {
		
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testReplace() {
		Сalc calc = new Сalc();
		assertEquals("replace method fail.", "s c t p n l", calc.replace("sqrt cos tg pow sin log"));
	}

	@Test
	public void testCheckForSpace() {
		Сalc calc = new Сalc();
		assertTrue("method checkForSpace fail.", calc.checkForSpace(' '));
		assertFalse("method checkForSpace fail.", calc.checkForSpace('d'));
	}
	
	@Test
	public void testCheckForOperator() {
		Сalc calc = new Сalc();
		assertTrue("method checkForOperator fail.", calc.checkForOperator('+'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('-'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('*'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('/'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('%'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('s'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('c'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('p'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('n'));
		assertTrue("method checkForOperator fail.", calc.checkForOperator('l'));
		
		assertFalse("method checkForOperator fail.", calc.checkForOperator(' '));
		assertFalse("method checkForOperator fail.", calc.checkForOperator('f'));
		assertFalse("method checkForOperator fail.", calc.checkForOperator('9'));
	}
	
	@Test
	public void testPriorityOperator() {
		Сalc calc = new Сalc();
		assertEquals(1, calc.priorityOperator('+')); assertEquals(1, calc.priorityOperator('-'));
		assertEquals(2, calc.priorityOperator('*')); assertEquals(2, calc.priorityOperator('/')); assertEquals(2, calc.priorityOperator('%'));
		assertEquals(3, calc.priorityOperator('s')); assertEquals(3, calc.priorityOperator('c')); assertEquals(3, calc.priorityOperator('t'));
		assertEquals(3, calc.priorityOperator('p')); assertEquals(3, calc.priorityOperator('n')); assertEquals(3, calc.priorityOperator('l'));
		assertEquals(-1, calc.priorityOperator(' '));
	}
	
	@Test
	public void testCalculationWithOperator() {
		Сalc calc = new Сalc();
		LinkedList<Double> list = new LinkedList<Double>(Arrays.asList(5.0, 6.0));
		calc.calculationWithOperator(list , '*');
		assertTrue(30.0 == list.getFirst());
		
		list.clear(); list.addFirst(5.0); list.addLast(6.0);
		calc.calculationWithOperator(list , '+');
		assertTrue(11.0 == list.getFirst());
		
		
		list.clear(); list.addFirst(5.0); list.addLast(6.0);
		calc.calculationWithOperator(list , '-');
		assertTrue(-1.0 == list.getFirst());
		
		list.clear(); list.addFirst(5.0); list.addLast(6.0);
		calc.calculationWithOperator(list , '/');
		assertTrue(5.0/6.0 == list.getFirst());
		
		
		list.clear(); list.addFirst(5.0); list.addLast(6.0);
		calc.calculationWithOperator(list , '%');
		assertTrue(5.0%6.0 == list.getFirst());
		
		list.clear(); list.addFirst(5.0); list.addLast(3.0);
		calc.calculationWithOperator(list , 'p');
		assertTrue(Math.pow(5.0, 3.0) == list.getFirst());
		
		list.clear(); list.addLast(16.0);
		calc.calculationWithOperator(list , 's');
		assertTrue(4.0 == list.getLast());
		
		list.clear(); list.addLast(90.0);
		calc.calculationWithOperator(list , 'c');
		assertTrue(Math.cos(90.0) == list.getLast());
		
		list.clear(); list.addLast(16.0);
		calc.calculationWithOperator(list , 't');
		assertTrue(Math.tan(16.0) == list.getLast());
		
		list.clear(); list.addLast(16.0);
		calc.calculationWithOperator(list , 'n');
		assertTrue(Math.sin(16.0) == list.getLast());
		
		list.clear(); list.addLast(16.0);
		calc.calculationWithOperator(list , 'l');
		assertTrue(Math.log(16.0) == list.getLast());
		
		list.clear();
	}
	
	@Test
	public void testEval() {
		Сalc calc = new Сalc();
		assertTrue(10 == calc.eval("((15-5)*5)/5"));
	}
	
	@Test
	public void testCalc() {
		ArrayList<String> testList = new ArrayList<String>(Arrays.asList("(57+1/tg(53))*sqrt(54-cos(23)) = 403.796753720054", "78*3-(65-34/3)/2 = 207.16666666666666"));
		ArrayList<String> inList = new ArrayList<String>(Arrays.asList("(57+1/tg(53))*sqrt(54-cos(23))", "78*3-(65-34/3)/2"));
		ArrayList<String> outList = new ArrayList<String>();
		new Сalc(inList, outList);
		assertEquals(testList, outList);
	}
}
