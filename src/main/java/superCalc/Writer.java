﻿package superCalc;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Writer {

	/** принимаем список и записываем его в файл */
	public void write(String filename, ArrayList<String> list) {
		String s = listInString(list);
		/** список перегоняем в файл */
		try {
			FileWriter fout = new FileWriter(filename);
			fout.write(s);
			fout.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** принимаем список, отдаём строку с переносами */
	public String listInString(ArrayList<String> list) {
		String s = "";
		for (int i = 0; i < list.size(); i++) {
			s = s + list.get(i) + "\n";
		}
		return s;
	}
}