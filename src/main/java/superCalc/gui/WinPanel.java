package superCalc.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import superCalc.Reader;
import superCalc.Writer;
import superCalc.СalcMultithreaded;

public class WinPanel extends JPanel {
	public JLabel L;
	//public JTextArea TA;
	private File file;
	private int ret = 1;
	JButton button;
	
	 WinPanel() {
		//добавление кнопки выбора файла
		JButton button2 = new JButton("Выбрать файл");
	        button2.setAlignmentX(CENTER_ALIGNMENT);
	        add(button2);
	 
	        button2.addActionListener(new ActionListener() {
	            public void actionPerformed(ActionEvent e) {
	                JFileChooser fileopen = new JFileChooser();             
	                ret = fileopen.showDialog(null, "Выбрать файл");
	                System.out.println("ret = "+ret);
	                System.out.println("JFileChooser.APPROVE_OPTION = "+JFileChooser.APPROVE_OPTION);
	                if (ret == JFileChooser.APPROVE_OPTION) {
	                    file = fileopen.getSelectedFile();
	                    L.setText(file.getAbsolutePath());
	                    button.setEnabled(true);
	                    System.out.println(ret);
	                }
	            }
	        });
		 
		 
		 //кнопка запускает вычисления
	        
	     button = new JButton("посчитать");   
		 add(button);
		 button.setEnabled(false);
		 button.addActionListener(listener);
		 
		 //это для вывода текста в окне программы
		 L = new JLabel("", JLabel.CENTER);
		 L.setForeground(Color.GREEN);
		 L.setBounds(20, 20, 20, 20);
		 add(L);
	 }
//вычисления под кнопкой
	 ActionListener listener = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
//-----------------------------			
			
			
			Thread t;
			t = Thread.currentThread();

			ArrayList<String> inList = new ArrayList<String>();
			ArrayList<String> outList = new ArrayList<String>();
			
			ArrayList<String> inList1 = new ArrayList<String>();
			ArrayList<String> inList2 = new ArrayList<String>();
			
			ArrayList<String> outList1 = new ArrayList<String>();
			ArrayList<String> outList2 = new ArrayList<String>();
			
			Reader reader = new Reader();
			try {
				reader.readFile(file.getAbsolutePath(), inList);//файл для чтения
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

	//разделяем		
			for (int i = 0; i < inList.size(); i=i+2) {
				inList1.add(inList.get(i));
			}
			for (int j = 1; j < inList.size(); j=j+2) {
				inList2.add(inList.get(j));
			}
			
			//new Сalc(inList, outList);//однопоточное вычисление
			new СalcMultithreaded(inList1, outList1);
			new СalcMultithreaded(inList2, outList2);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//складываем вместе		
					for (int k = 0; k < outList1.size(); k++) {
						outList.add(outList1.get(k));
						
						if (k < outList2.size()) {outList.add(outList2.get(k));}
						
					}
			
			Writer writer = new Writer();
			writer.write(file.getParent()+"\\answers.txt", outList);//файл для записи ответа
			
			for (int j = 0; j < outList.size(); j++) {
				System.out.println(j+1+": "+outList.get(j));
			}
			System.out.println("work completed");			
			
			L.setText("complete");
//-----------------------------			
		}
	};
	 
}
