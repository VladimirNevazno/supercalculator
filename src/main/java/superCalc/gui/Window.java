package superCalc.gui;

import javax.swing.JFrame;

public class Window extends JFrame {
//constructor	
	public Window() {
		setTitle("supercalculator");
		setSize(300, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(50, 50);
		setVisible(true);
		
		WinPanel panel = new WinPanel();
		add(panel);
		
	}

}
