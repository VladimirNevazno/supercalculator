package superCalc;

import java.util.ArrayList;
import java.util.LinkedList;

public class СalcMultithreaded extends Thread {
	private ArrayList<String> inList = new ArrayList<String>();
	private ArrayList<String> outList = new ArrayList<String>();
	
	public СalcMultithreaded() {
		super();
	}
	
	/** в конструктор приходит список мат.задач, отдаётся список мат. задач+решение */
	public СalcMultithreaded(ArrayList<String> list, ArrayList<String> outList) {
		this.inList = list;
		this.outList = outList;
		
		start();
	}

	public void run() {
		eval(inList, outList);
	}
	
	public void eval(ArrayList<String> list, ArrayList<String> outList) {
		for (int i = 0; i < list.size(); i++) {
			Double y = eval(list.get(i));
			outList.add(list.get(i) + " = " + y);
		}
	}

	public Double eval(String s) {
		LinkedList<Double> st = new LinkedList<Double>(); /** сюда наваливают цифры */
		LinkedList<Character> op = new LinkedList<Character>();
		/** сюда опрераторы и st и op в порядке поступления */
		s = replace(s);
		for (int i = 0; i < s.length(); i++) {
			/** парсим строку с выражением и вычисляем */
			char c = s.charAt(i); /** берём символ */
			if (checkForSpace(c)) /** check for space */
				continue;
			if (c == '(')
				op.add('(');
			else if (c == ')') {
				while (op.getLast() != '(')
					calculationWithOperator(st, op.removeLast());
				op.removeLast();
			} else if (checkForOperator(c)) {
				while (!op.isEmpty()
						&& priorityOperator(op.getLast()) >= priorityOperator(c))
					calculationWithOperator(st, op.removeLast());
				op.add(c);
			} else if (Character.isDigit(s.charAt(i))) {
				String operand = "";
				while (i < s.length() && Character.isDigit(s.charAt(i)))
					operand += s.charAt(i++);
				--i;
				st.add(Double.parseDouble(operand));
			} else
				return 0.0;
		}
		while (!op.isEmpty())
			calculationWithOperator(st, op.removeLast());
		return st.get(0);
	}

	public String replace(String s) {
		s = s.replaceAll("sqrt", "s");
		s = s.replaceAll("cos", "c");
		s = s.replaceAll("tg", "t");
		s = s.replaceAll("pow", "p");
		s = s.replaceAll("sin", "n");
		s = s.replaceAll("log", "l");
		return s;

	}

	public boolean checkForSpace(char c) { /** true if space */
		return c == ' ';
	}

	public boolean checkForOperator(char c) {
		return c == '+' || c == '-' || c == '*' || c == '/' || c == '%'
				|| c == 's' || c == 'c' || c == 't' || c == 'p' || c == 'n'
				|| c == 'l';
	}

	public int priorityOperator(char op) {
		switch (op) {
		case '+':
		case '-':
			return 1;
		case '*':
		case '/':
		case '%':
			return 2;
		case 's':
		case 'c':
		case 't':
		case 'p':
		case 'n':
		case 'l':
			return 3;
		default:
			return -1;
		}
	}

	public void calculationWithOperator(LinkedList<Double> st, char op) {
		if (op == '+' || op == '-' || op == '*' || op == '/' || op == '%'
				|| op == 'p' && st.getLast() != null && st.getFirst() != null) {
			Double r = st.removeLast();
			Double l = st.removeLast();
			switch (op) { /** выполняем действие между l и r в зависимости от */
			case '+':
				st.add(l + r);
				break;
			case '-':
				st.add(l - r);
				break;
			case '*':
				st.add(l * r);
				break;
			case '/':
				st.add(l / r);
				break;
			case '%':
				st.add(l % r);
				break;
			case 'p':
				st.add(Math.pow(l, r));
				break;
			}
		}
		if (op == 's' || op == 'c' || op == 't' || op == 'n' || op == 'l'
				&& st.getLast() != null) {
			Double r = st.removeLast();
			switch (op) { /** выполняем действие между l и r в зависимости от оператора */
			case 's':
				st.add(Math.sqrt(r));
				break;
			case 'c':
				st.add(Math.cos(r));
				break;
			case 't':
				st.add(Math.tan(r));
				break;
			case 'n':
				st.add(Math.sin(r));
				break;
			case 'l':
				st.add(Math.log(r));
				break;
			}
		}

	}

}
