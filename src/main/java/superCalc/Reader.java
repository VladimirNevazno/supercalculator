﻿package superCalc;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/** этот класс считывает файл в ArrayList<String>,
 * где каждый элемент является математическим примером, который надо решить.
 */
public class Reader {
	
	
	public void readFile(String fileName, ArrayList<String> list) throws IOException {
		
			String s;
			FileInputStream fin = new FileInputStream(fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(fin));

			s = br.readLine();/** first string */
			s = s.toLowerCase();
//cycle beginning
			for (; s != null;) {
				try {
					list.add(s);
					s = br.readLine();/** next string */
				} catch (NullPointerException e) {
					break;
				}
			}
//end cycle			
			fin.close();
		
	}
}
